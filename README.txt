DOGE Tilemap Editor

A simple tilemap editor for DOGE.
Usage:

	TilemapEditor tile_size tileset_filename [tilemap_name]


Mode 1: Tile selection.
Click on a tile from your tileset to select it.
This will automatically move you into mode 2.

Mode 2: Tilemap editor.
The tilemap is displayed.
Click a place on it to set it to the currently selected tile.
This will be previewed in the bottom-right-hand corner of the window.
Press the c key to return to mode 1 in order to select a different tile.

Finishing:
Close the window to output the tilemap as an array of doge_v2ui items.

The width and height, as well as an array holding the result are generated.
It currently outputs in C header format to stdout.
I might add more formats if I implement them into DOGE.
This is probably useless.
