#include <doge2d.h>
#include <stdbool.h>
#include <errno.h>

#define LAYOUT_DIM 1024

#define WIN_H 720
#define WIN_W 1280

#define ACTIVE_SCALE 5.0f

enum EditingStatus {
    STAT_DRAWING,
    STAT_MOVING,
    STAT_SELECTING,
};

void put_at(Uint32* layout, Uint32 x, Uint32 y, Uint32 tilemap_width, Uint32 tile_size, Uint32 tile_sel_x, Uint32 tile_sel_y, Uint32* tile_max_x, Uint32* tile_max_y);
Uint32 max(Uint32 a, Uint32 b);

void put_at_vec(doge_Tilemap* tilemap, doge_v2ui pos, doge_v2ui tile_sel, doge_v2ui* tile_max);
