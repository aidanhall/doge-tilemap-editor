#include "main.h"

Uint32 max(Uint32 a, Uint32 b) {
    return (a > b) ? a : b;
}
void put_at(Uint32* layout, Uint32 x, Uint32 y, Uint32 tilemap_width, Uint32 tile_size, Uint32 tile_sel_x, Uint32 tile_sel_y, Uint32* tile_max_x, Uint32* tile_max_y) {
    fprintf(stderr, "(%d,%d)\n", x, y);
    Uint32 tile_ind_x =  x / tile_size;
    Uint32 tile_ind_y =  y / tile_size;
    *tile_max_x = max(*tile_max_x, tile_ind_x);
    *tile_max_y = max(*tile_max_y, tile_ind_y);
    layout[2*(tile_ind_y*tilemap_width + tile_ind_x)] = tile_sel_x;
    layout[2*(tile_ind_y*tilemap_width + tile_ind_x) + 1] = tile_sel_y;
}

void put_at_vec(doge_Tilemap* tilemap, doge_v2ui pos, doge_v2ui tile_sel, doge_v2ui* tile_max) {
    fprintf(stderr, "(%d,%d)\n", pos.x, pos.y);
    doge_v2ui tile_ind = doge_v2ui_mul(pos, 1.0f/tilemap->tileset->tile_size);
    tile_max->x = max(tile_max->x, tile_ind.x);
    tile_max->y = max(tile_max->y, tile_ind.y);

    tilemap->layout[tile_ind.y*tilemap->width + tile_ind.x] = tile_sel;
}

int main(int argc, char* argv[]) {

    Uint32 tile_size;
    char tileset_filename[BUFSIZ];
    char tilemap_data_name[BUFSIZ] = "tilemap";
    if (argc <= 2) {
	fprintf(stderr, "Usage: %s tile_size tileset_file [tilemap_name]\n", argv[0]);
	exit(1);
    } else {
	if (sscanf(argv[1], "%d", &tile_size) < 1) {
	    fprintf(stderr, "Couldn't parse tile size.\n");
	    exit(1);
	} else if (sscanf(argv[2], "%s", tileset_filename) < 1) {
	    fprintf(stderr, "Couldn't parse tileset filename.\n");
	    exit(1);
	} else if (argc >= 4) {
	    if (sscanf(argv[3], "%s", tilemap_data_name) < 1) {
		fprintf(stderr, "Couldn't load tilemap name.\n");
		exit(1);
	    }
	} else
	    fputs("All good, no name.", stderr);
    }

    doge_init();
    SDL_Window* win = SDL_CreateWindow("Tilemap Editor", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIN_W, WIN_H, SDL_WINDOW_SHOWN);
    doge_configure_window(win);
    SDL_Renderer* ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
    doge_configure_renderer(ren);


    /* Load tileset. */
    doge_Tileset* tileset = doge_create_tileset(tileset_filename, tile_size, 1.0f, ren);
    if (tileset == NULL) {
	SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Creating tileset failed: %s\n", SDL_GetError());
	return doge_exit_with_sdl(1);
    }

    /* Initialise layout and tilemap. */
    doge_v2ui* layout = SDL_calloc(LAYOUT_DIM*LAYOUT_DIM, sizeof(doge_v2ui));
    doge_Tilemap* tilemap =
	doge_create_tilemap(tileset, layout, LAYOUT_DIM,LAYOUT_DIM);

    bool quit = false;
    enum EditingStatus status = STAT_SELECTING;
    SDL_Event event;
    doge_v2ui tile_sel = {1,1};
    doge_v2ui tile_max = {0,0};
    const doge_v2ui tile_blank = {0,0}; /* The coordinates of the blank tile. Should probably be customisable. */
    const SDL_Rect dst = {.x = WIN_W - (ACTIVE_SCALE*tile_size)-10, .y = WIN_H-(ACTIVE_SCALE*tile_size)-10,
	.w =ACTIVE_SCALE*tile_size, .h = ACTIVE_SCALE*tile_size};

    while (!quit) {
	while (SDL_PollEvent(&event)) {
	    switch (event.type) {
		case SDL_QUIT:
		    quit = true;
		    break;
		case SDL_MOUSEBUTTONDOWN:
		    {
			const struct doge_v2ui pos = {
			    .x = event.button.x,
			    .y = event.button.y,
			};
			switch (status) {
			    case STAT_MOVING:
				status = STAT_DRAWING;
				put_at_vec(tilemap, pos,
					(event.button.button == SDL_BUTTON_LEFT) ? tile_sel : tile_blank,
					&tile_max );
				break;
			    case STAT_SELECTING:
				tile_sel = doge_v2ui_mul(pos, 1.0f/tile_size);
				status = STAT_MOVING;
				break;
			    default:
				break;
			}
		    }
		    break;
		case SDL_MOUSEMOTION:
		    if (status == STAT_DRAWING){
			put_at_vec(tilemap, (doge_v2ui){.x=event.button.x,.y=event.button.y},
				(event.button.button == SDL_BUTTON_LEFT) ? tile_sel : tile_blank,
				&tile_max );
		    }
		    break;
		case SDL_MOUSEBUTTONUP:
		    if (status == STAT_DRAWING) {
			status = STAT_MOVING;
		    }
		    break;
		case SDL_KEYDOWN:
		    switch (event.key.keysym.scancode) {
			case SDL_SCANCODE_C:
			    fprintf(stderr, "Pressed c.\n");
			    if (status == STAT_SELECTING) {
				status = STAT_MOVING;
			    } else {
				status = STAT_SELECTING;
			    }
			    break;
			default:
			    break;
		    }
		    break;
		default:
		    break;
	    }
	}
	SDL_RenderClear(ren);
	/* Render tilemap. */
	switch (status) {
	    case STAT_DRAWING:
	    case STAT_MOVING:
		doge_render_tilemap(tilemap, 0, 0, ren);
		break;
	    case STAT_SELECTING:
		doge_render_texture(ren, tileset->tiles, 0, 0);
	    default:
		break;
	}

	/* Show active tile. */

	SDL_Rect src = {.x = tile_sel.x*tile_size, .y = tile_sel.y*tile_size, .w=tile_size, .h=tile_size};
	SDL_RenderCopy(ren, tileset->tiles, &src, &dst);
	SDL_RenderPresent(ren);
    }

    /* Output Level to stdout. */
    tile_max = doge_v2ui_add(tile_max, (doge_v2ui){.x=1, .y=1});
    fprintf(stderr,"Dimensions: %d, %d\n", tile_max.x, tile_max.y);

    printf("const Uint32 %s_width = %d\nconst Uint32 %s_height = %d\n", tilemap_data_name, tile_max.x,tilemap_data_name,  tile_max.y);
    printf("struct doge_v2ui %s[] = {\n", tilemap_data_name);

    for (Uint32 row = 0; row < tile_max.y; ++row) {
	for (Uint32 column = 0; column < tile_max.x; ++column) {
	    printf("\t{ %d,%d, },", tilemap->layout[(row*tilemap->width)+column].x, tilemap->layout[(row*tilemap->width)+column].y);
	}
	putchar('\n');
    }

    puts("};");
    doge_destroy_tileset(tileset);
    doge_destroy_tilemap(tilemap);
    return doge_exit_with_sdl(0);
}
